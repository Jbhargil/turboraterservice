﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurboRater.Insurance;

namespace TurboRater.ApiClients.Imp
{
  public class QuoteTemplate
  {
    /// <summary>
    /// Primary Key
    /// </summary>
    public int RecordId { get; set; }

    /// <summary>
    /// Guid that identifies a specific quote template. 
    /// This is what we expose to users rather than recordId's for tighter security. 
    /// </summary>
    public Guid QuoteTemplateId { get; set; }

    /// <summary>
    /// Date the template was created. 
    /// </summary>
    public DateTime DateCreated { get; set; }

    /// <summary>
    /// Date the template was last modified. 
    /// </summary>
    public DateTime LastModifiedDate { get; set; }

    /// <summary>
    /// Guid that identifies the user that created the template. 
    /// </summary>
    public Guid CreatedByUserId { get; set; }

    /// <summary>
    /// Guid that identifies the user that last modified the template. 
    /// </summary>
    public Guid LastModifiedUserId { get; set; }

    /// <summary>
    /// List of quote template property items
    /// </summary>
    public List<QuoteTemplateItem> QuoteTemplateItems { get; set; }

    /// <summary>
    /// Line of insurance for this template.
    /// </summary>
    public InsuranceLine LineOfInsurance { get; set; }

    /// <summary>
    /// State the quote template is in
    /// </summary>
    public USState State { get; set; }

    /// <summary>
    /// Identifies the agency for which this quote template was created. 
    /// </summary>
    public Guid AgencyId { get; set; }

    /// <summary>
    /// Identifies the agency location for which this quote template was created. 
    /// </summary>
    public Guid AgencyLocationId { get; set; }

    /// <summary>
    /// Name of the Quote Template 
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Policy Record ID of the old policy template from
    /// which this template was converted
    /// If this template was not converted, this will be -1.
    /// </summary>
    public int ConvertedFromQuoteID { get; set; }
  }
}