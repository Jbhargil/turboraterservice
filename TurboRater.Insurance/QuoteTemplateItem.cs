﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TurboRater.Insurance;

namespace TurboRater.ApiClients.Imp
{
  public class QuoteTemplateItem
  {
    /// <summary>
    /// Primary Key, the id of the quote template item
    /// </summary>
    public int RecordId { get; set; }

    /// <summary>
    /// Foreign key for the quote template. 
    /// </summary>
    public int QuoteTemplateLinkId { get; set; }


    /// <summary>
    /// Field name of the Quote Template Item 
    /// </summary>
    public string FieldName { get; set; }

    /// <summary>
    /// If the template item calls for a default value, this is the default value we use. 
    /// </summary>
    public string DefaultValue { get; set; }

    /// <summary>
    /// List of booleans to track if an action has been taken. 
    /// This is not stored with the quote templated. 
    /// It will be stored when the template object is serialized and saved to a policy for which it is being used. 
    /// </summary>
    public List<bool> UserActionTaken { get; set; }

    /// <summary>
    /// Scope of the template item 
    /// Policy / Driver / Car / etc. 
    /// </summary>
    public ItemScope Scope { get; set; }

    /// <summary>
    /// What type of quote template action to take for this time. 
    /// Show / Blank and Required / Hide 
    /// </summary>
    public QuoteTemplateAction Action { get; set; }
  }
}
