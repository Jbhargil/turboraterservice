﻿// -----------------------------------------------------------------------
// <summary>
// A class that contains wraps premium valuation request. 
// </summary>
// <copyright file="PremiumValuationRequest.cs" company="ITC">
// Copyright ITC. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TurboRater.ApiClients.Imp
{
  using System.ComponentModel.DataAnnotations;
  using TurboRater.Insurance.HO;

  public class PremiumValuationRequest
  {
    /// <summary>
    /// address1 of the property for the premium valuation
    /// </summary>
    [Required]
    public string Address1 { get; set; }

    /// <summary>
    /// address2 of the property (if applicable) for the premium valuation
    /// </summary>
    public string Address2 { get; set; }

    /// <summary>
    /// city of the property for the premium valuation
    /// </summary>
    [Required]
    public string City { get; set; }

    /// <summary>
    /// state of the property for the premium valuation
    /// </summary>
    [Required]
    public string State { get; set; }

    /// <summary>
    /// zipcode of the property for the premium valuation
    /// </summary>
    [Required]
    public string ZipCode { get; set; }

    /// <summary>
    /// year of construction of the property for the premium valuation
    /// </summary>
    [Required]
    public int YearOfConstruction { get; set; }

    /// <summary>
    /// square footage of the property for the premium valuation
    /// </summary>
    [Required]
    public int SquareFootage { get; set; }

    /// <summary>
    /// # of stories of the property for the premium valuation
    /// </summary>
    public int NumberOfStories { get; set; }

    /// <summary>
    /// # of families of the property for the premium valuation
    /// </summary>
    [Required]
    public int NumberOfFamilies { get; set; }

    /// <summary>
    /// story type of the property for the premium valuation
    /// </summary>
    [Required]
    public StoryType StoryType { get; set; }

  }
}
