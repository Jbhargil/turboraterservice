﻿// -----------------------------------------------------------------------
// <summary>
// A class that contains wraps premium valuation response. 
// </summary>
// <copyright file="PremiumValuationResponse.cs" company="ITC">
// Copyright ITC. All rights reserved.
// </copyright>
// -----------------------------------------------------------------------

namespace TurboRater.ApiClients.Imp
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using TurboRater.Insurance.HO;

  public class PremiumValuationResponse
  {
    /// <summary>
    /// address1 of the property for the premium valuation
    /// </summary>
    public string Address1 { get; set; }

    /// <summary>
    /// address2 of the property for the premium valuation
    /// </summary>
    public string Address2 { get; set; }

    /// <summary>
    /// city of the property for the premium valuation
    /// </summary>
    public string City { get; set; }

    /// <summary>
    /// state of the property for the premium valuation
    /// </summary>
    public string State { get; set; }

    /// <summary>
    /// zipcode of the property for the premium vaulation
    /// </summary>
    public string ZipCode { get; set; }

    /// <summary>
    /// # of families of the property for the premium vaulation
    /// </summary>
    public int NumberOfFamilies { get; set; }

    /// <summary>
    /// story type of the property for the premium vaulation
    /// </summary>
    public string StoryType { get; set; }

    /// <summary>
    /// year of construction of the property for the premium vaulation
    /// </summary>
    public int YearOfConstruction { get; set; }

    /// <summary>
    /// square footage of the property for the premium vaulation
    /// </summary>
    public int SquareFootage { get; set; }

    /// <summary>
    /// foundation type of the property for the premium vaulation
    /// </summary>
    public string FoundationType { get; set; }

    /// <summary>
    /// roof composision of the property for the premium vaulation
    /// </summary>
    public string RoofComposition { get; set; }

    /// <summary>
    /// hail resistante roof of the property for the premium vaulation?
    /// </summary>
    public bool HailResistantRoof { get; set; }

    /// <summary>
    /// shape of the roof of the property for the premium vaulation
    /// </summary>
    public string RoofShape { get; set; }

    /// <summary>
    /// primary heating type of the property of the premium vaulation
    /// </summary>
    public string PrimaryHeatingType { get; set; }

    /// <summary>
    /// secondary heating type (if exists) of the property for the premium vaulation
    /// </summary>
    public string SecondaryHeatingType { get; set; }

    /// <summary>
    /// # of full bathrooms of the property for the premium vaulation
    /// </summary>
    public int NumberOfFullBaths { get; set; }

    /// <summary>
    /// # of 3/4 bathrooms of the property for the premium vaulation
    /// </summary>
    public int NumberOfThreeQuarterBaths { get; set; }

    /// <summary>
    /// # of 1/2 bathrooms of the property for the premium vaulation
    /// </summary>
    public int NumberOfHalfBaths { get; set; }

    /// <summary>
    /// dwelling amount of the property for the premium vaulation
    /// </summary>
    public int DwellingAmt { get; set; }

    /// <summary>
    /// construction (brick/frame/etc...) of the property for the premium vaulation
    /// </summary>
    public string Construction { get; set; }

    /// <summary>
    /// swimming pool exists of the property for the premium vaulation?
    /// </summary>
    public bool SwimmingPool { get; set; }

    /// <summary>
    /// swimming pool type (above/in ground) of the property for the premium vaulation
    /// </summary>
    public string SwimmingPoolType { get; set; }

    public bool WasSuccessful { get; set; }

    public List<string> Messages { get; set; }
  }
}
